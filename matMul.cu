#include "matMul.h"


__global__ void kernelMatMult(float *A, float *B, float *C, int n) {
    int bx = blockIdx.x;     
    int by = blockIdx.y;
    int tx = threadIdx.x;        
    int ty = threadIdx.y;           
    
    int aInd = n * (bx * BLOCK_SIZE  + tx);
    int bInd = by * BLOCK_SIZE + ty;
    int cInd = n * (bx * BLOCK_SIZE + tx) + (by * BLOCK_SIZE + ty); 
    
    float sum = 0.f;
    
    for (int i = 0; i < n; i++) {
      sum += A[aInd + i] * B[bInd + i];
    }
    
    C[cInd] = sum;
}

void matMulCuda(float *A, float *B, float *C, int n) {
  //размер сетки (64х64 блока)
  dim3 gridSize(N / BLOCK_SIZE, N / BLOCK_SIZE);
  //размер блока (16х16)
  dim3 blockSize(BLOCK_SIZE, BLOCK_SIZE);
  //вызов функции ядра
  kernelMatMult<<<gridSize, blockSize>>>(A, B, C, N);
}
