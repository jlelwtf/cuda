#ifndef MATMUL
#define MATMUL
#define N (1024)
#define BLOCK_SIZE (16)
void matMulCuda(float *A, float *B, float *C, int n);
#endif