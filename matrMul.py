import numpy as np
from pycuda import driver, compiler, gpuarray, tools
import pycuda.autoinit

kernel_code = '''
__global__ void matMult(float* A, float* B, float* C, int n) {
    int bx = blockIdx.x;     
    int by = blockIdx.y;
    int tx = threadIdx.x;        
    int ty = threadIdx.y;           
    
    int aInd = n * (bx * %(BLOCK_SIZE)s  + tx);
    int bInd = by * %(BLOCK_SIZE)s + ty;
    int cInd = n * (bx * %(BLOCK_SIZE)s + tx) + (by * %(BLOCK_SIZE)s + ty); 
    
    float sum = 0.f;
    
    for (int i = 0; i < n; i++) {
      sum += A[aInd + i] * B[bInd + i];
    }
    
    C[cInd] = sum;
}
'''
N = 1024
BLOCK_SIZE = 16

hA = np.full(N * N, 1.0).astype(np.float32)
hB = np.full(N * N, 2.0).astype(np.float32)

dA = gpuarray.to_gpu(hA)
dB = gpuarray.to_gpu(hB)
dC = gpuarray.to_gpu(hB)

kernel_code = kernel_code % {'BLOCK_SIZE': BLOCK_SIZE}
kernel = compiler.SourceModule(kernel_code)
matMult = kernel.get_function("matMult")

grid = (N / BLOCK_SIZE, N / BLOCK_SIZE, 1)
block = (BLOCK_SIZE, BLOCK_SIZE, 1)

matMult(dA, dB, dC, np.int32(N), grid=grid, block=block)

print(dC.get()) 
