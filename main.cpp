#include <iostream>
#include <cuda_runtime_api.h>
#include "matMul.h"

void constantInit(float *data, int size, float val) {
    for (int i = 0; i < size; ++i) {
        data[i] = val;
    }
}
void matMult() {

  float* hA = new float[N * N];
  float* hB = new float[N * N];
  float* hC = new float[N * N];
  constantInit(hA, N * N, 1.f);
  constantInit(hB, N * N, 2.f);
   
  float* dA;
  float* dB;
  float* dC;
  
  int numBytes = N * N * sizeof(float);
  //выделение памяти на устройстве
  cudaMalloc((void **)&dA, numBytes);
  cudaMalloc((void **)&dB, numBytes);
  cudaMalloc((void **)&dC, numBytes);
  //копирование матриц на усройство
  cudaMemcpy(dA, hA, numBytes, cudaMemcpyHostToDevice);
  cudaMemcpy(dB, hB, numBytes, cudaMemcpyHostToDevice);
  matMulCuda(dA, dB, dC, N);
  cudaMemcpy(hC, dC, numBytes, cudaMemcpyDeviceToHost);
  cudaFree(dA);
  cudaFree(dB);
  cudaFree(dC);
  
  //вывод результат (не вся, верхний левый блок 10х10)
  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 10; j++) {
      std::cout << hC[i + j * N] << " ";
    }
    std::cout << std::endl;
  }
  delete hA;
  delete hB;
  delete hC; 
}

int main() {
  int dev_count;
  cudaDeviceProp deviceProp;
  cudaGetDeviceCount(&dev_count);
  std::cout << std::endl;
  for(int dev_ind = 0; dev_ind < dev_count; ++dev_ind) {
    cudaDeviceProp deviceProperties;
    cudaGetDeviceProperties(&deviceProperties, dev_ind);
    std::cout << "\t" << dev_ind << " " << deviceProperties.name << std::endl;  
  }
  std::cout << std::endl << "choose dev ind: ";
  int dev_ind;
  std::cin >> dev_ind;
  std::cout << std::endl;
  cudaError_t error;
  error = cudaGetDeviceProperties(&deviceProp, dev_ind);
  if (error == cudaSuccess) {
    cudaSetDevice(dev_ind);
    matMult();
  }
  else {
    std::cout << "wrond id" << std::endl;
  }
  return 0;
}